//2019.3.12
//https://blog.csdn.net/lym940928/article/details/79787548
class Solution {
public:
    vector<string> subdomainVisits(vector<string>& cpdomains) {
        unordered_map<string, int> domainRecord;
        vector<string> resDomain;
        for(auto domain : cpdomains){
            int i = domain.find(" ");
            int num = stoi(domain.substr(0, i));
            string eachDomain = domain.substr(i+1, domain.size()-i-1);
            for(int i = eachDomain.size()-1; i>=0; i--){
                if(eachDomain[i] == '.'){
                    domainRecord[eachDomain.substr(i+1, eachDomain.size()-i-1)] += num;
                }
                else if(i == 0){
                    domainRecord[eachDomain.substr(0, eachDomain.size())] += num;
                }
            }
        }
        for(auto domain : domainRecord){
            resDomain.push_back(to_string(domain.second)+" "+domain.first);
        }
        return resDomain;
    }
};

//Runtime: 20 ms, faster than 99.40% of C++ online submissions for Subdomain Visit Count.
//Memory Usage: 13.5 MB, less than 60.81% of C++ online submissions for Subdomain Visit Count.
