//https://leetcode.com/problems/positions-of-large-groups/discuss/128957/C%2B%2BJavaPython-Straight-Forward
//https://yeqiuquan.blogspot.com/2018/05/830-positions-of-large-groups.html
class Solution {//2019.1.7
public:
    vector<vector<int>> largeGroupPositions(string S) {
        vector<vector<int>> res;
        int left = 0;
        int right = 0;
        while(left < S.size()){
            while(right < S.size() && S[left] == S[right]){
                right++;
            }
            if(right - left >= 3) res.push_back({left, right-1});
            left = right;
        }
        return res;
    }
};
