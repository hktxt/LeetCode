//https://leetcode.com/problems/flipping-an-image/discuss/130590/C%2B%2BJavaPython-Reverse-and-Toggle
class Solution {//2019.1.7
public:
    vector<vector<int>> flipAndInvertImage(vector<vector<int>>& A) {
        for(auto & row : A){ 
            reverse(row.begin(), row.end());
        }
        for(auto & row : A){ 
            for (int & i: row){
                i ^= 1;
            }
        }
        return A;
    }
};
