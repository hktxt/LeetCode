// https://leetcode.com/problems/degree-of-an-array/discuss/124317/C%2B%2BJavaPython-one-pass-and-O(M)-space
class Solution {//2019.1.4
public:
    int findShortestSubArray(vector<int>& nums) {
        unordered_map<int, int> first, counter;
        int res = 0, degree = 0;
        for(int i=0; i<nums.size(); i++){
            if(first.count(nums[i]) == 0) first[nums[i]] = i;
            if(++counter[nums[i]] > degree){
                degree = counter[nums[i]];
                res = i - first[nums[i]] + 1;
            }else if(counter[nums[i]] == degree){
                res = min(res, i - first[nums[i]] + 1);
            }
        }
        return res;
    }
};
