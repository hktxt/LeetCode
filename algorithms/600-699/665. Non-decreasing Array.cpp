//https://www.youtube.com/watch?v=GtQHTxCrLHI
// 1) 2 4 2 6
// 2) 3 4 2 6
class Solution {//2019.1.4
public:
    bool checkPossibility(vector<int>& nums) {
        int count = 0;
        for(int i=1; i<nums.size(); i++){
            if(nums[i] < nums[i-1]){
                count++;
                if(count > 1) return false;
                if(i - 2 < 0 || nums[i-2] <= nums[i]){
                    nums[i-1] = nums[i];
                }else{
                    nums[i] = nums[i-1];
                }
            }
        }
        return true;
    }
}; 
