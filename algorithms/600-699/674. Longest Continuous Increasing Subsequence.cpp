class Solution {//2019.1.4, walkthrough the nums, DP, 16 ms, faster than 35.33%
public:
    int findLengthOfLCIS(vector<int>& nums) {
        
        if(nums.empty()) return 0;
        if(nums.size() == 1) return 1;
        int max_length = 1, cur_length = 1;
        for(int i=0; i<nums.size()-1; i++){
            if(nums[i] < nums[i+1]){
                cur_length++;
            }else{
                cur_length = 1;
            }
            max_length = max(max_length, cur_length);
        }
        return max_length;
    }
};

//16 ms, faster than 35.33%
class Solution {//2019.1.4
public:
    int findLengthOfLCIS(vector<int>& nums) {
        
        if(nums.empty()) return 0;
        int max_length = 1, cur_length = 1;
        for(int i=1; i<nums.size(); i++){
            if(nums[i] > nums[i-1]){
                cur_length++;
                max_length = max(max_length, cur_length);
            }else{
                cur_length = 1;
            }
        }
        return max_length;
    }
};
