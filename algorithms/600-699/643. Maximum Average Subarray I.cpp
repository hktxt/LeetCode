//O(n*k)
/**
class Solution {//2019.1.4
public:
    double findMaxAverage(vector<int>& nums, int k) {
        int max_sum = INT_MIN;
        int n = nums.size();
        for(int i=0; i<n-k+1; i++){
            int cur_sum = 0;
            for(int j=0; j<k; j++){
                cur_sum += nums[i+j];
            }
            max_sum = max(max_sum, cur_sum);
        }
        return double(max_sum)/k;
    }
};**/


//sliding windows, DP, https://www.geeksforgeeks.org/window-sliding-technique/
class Solution {//O(n)
public:
    double findMaxAverage(vector<int>& nums, int k) {
        int n = nums.size();
        int max_sum = 0;
        for(int i=0; i<k; i++){
            max_sum += nums[i];
        }
        int cur_sum = max_sum;
        for(int i=k; i<n; i++){
            cur_sum += nums[i] - nums[i-k];
            max_sum = max(max_sum, cur_sum);
        }
        return double(max_sum)/k;
    }
}; 
