//I hate this one, check solution blew:
//https://leetcode.com/problems/image-smoother/discuss/188754/C%2B%2B-0(1)-space-with-in-place-update-~100ms-(beats-99)
class Solution {
    void sumRow(vector<int>& row, const int& colCount) {
        int prev = 0;
        for (int col = 0; col < colCount - 1; col++) {
            int tmp = row[col];
            row[col] += prev + row[col+1];
            prev = tmp;
        }
        row[colCount - 1] += prev;
    }
    void sumCol(vector<vector<int>>& M, const int rowCount, const int& col) {
        int prev = 0;
        for (int row = 0; row < rowCount - 1; row++) {
            int tmp = M[row][col];
            M[row][col] += prev + M[row+1][col];
            prev = tmp;
        }
        M[rowCount - 1][col] += prev;
    }
    int getNeighborWithSelfCount(const int& rowCount, const int& colCount, const int& row, const int& col) {
        // special cases
        if (rowCount == 1) {
            if (colCount == 1) {
                // leave this cell alone
                return 1;
            }
            else {
                if (col == 0 || col == colCount - 1) {
                    // first/last in row
                    return 2;
                }
                else {
                    // middle in the row
                    return 3;
                }
            }
        }
        else if (colCount == 1) {
            if (row == 0 || row == rowCount - 1) {
                // first/last in col
                return 2;
            }
            else {
                // middle in the col
                return 3;
            }
        }
        
        // normal matrix;
        if (row == 0 || row == rowCount - 1) {
            if (col == 0 || col == colCount - 1) {
                // corners
                return 4;
            }
            else {
                // top or bottom (not corners)
                return 6;
            }
        }
        else {
            if (col == 0 || col == colCount - 1) {
                // left or right (not corners)
                return 6;
            }
            else {
                // middle cells
                return 9;
            }
        }
    }
    void divByNeighborCount(vector<vector<int>>& M, const int& rowCount, const int& colCount) {
        for (int row = 0; row < rowCount; row++) {
            for (int col = 0; col < colCount; col++) {
                int neighborWithSelfCount = getNeighborWithSelfCount(rowCount, colCount, row, col);
                M[row][col] /= neighborWithSelfCount;
            }
        }
    }
public:
    vector<vector<int>> imageSmoother(vector<vector<int>>& M) {
        const int rowCount = M.size();
        if (rowCount == 0) {
            return {};
        }
        const int colCount = M[0].size();
        if (colCount == 0) {
            return {};
        }
        
        for (int row = 0; row < rowCount; row++) {
            sumRow(M[row], colCount);
        }
        
        for (int col = 0; col < colCount; col++) {
            sumCol(M, rowCount, col);
        }
        
        divByNeighborCount(M, rowCount, colCount);
        return move(M);
    }
};
