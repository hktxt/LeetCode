//贪心，从左往右种花，如果当前为空地（0），且左右不为花（1）则能种花，种之，n--；不然则不能种，avil=0；
class Solution {//2019.1.3
public:
    bool canPlaceFlowers(vector<int>& flowerbed, int n) {
        int size = flowerbed.size();
        for(int i=0; i<size; i++){
            if(flowerbed[i] == 0){
                bool avil = 1;
                if(i - 1 >= 0 && flowerbed[i-1] == 1) avil = 0;
                if(i + 1 < size && flowerbed[i+1] == 1) avil = 0;
                if(avil) flowerbed[i] = 1, n--;
            }
        }
        return n <= 0;
    }
}; 
