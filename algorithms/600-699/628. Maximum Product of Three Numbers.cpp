/**
class Solution {//排序，降序，前三个为最大值；陷阱：负数情况
public:
    int maximumProduct(vector<int>& nums) {
        int n = nums.size();
        sort(nums.begin(), nums.end(), greater<int>());//降序
        
        return max(nums[0]*nums[1]*nums[2], nums[0]*nums[n-1]*nums[n-2]);
    }
};//Runtime: 80 ms, faster than 5.26% of C++ online submissions for Maximum Product of Three Numbers.
**/

//包含负数情况，只要求出最大的三个数和最小的两个数即可
class Solution {
public:
    int maximumProduct(vector<int>& nums) {
        //定义最大三个和最小2个
        int MAX1 = INT_MIN, MAX2 = INT_MIN, MAX3 = INT_MIN;
        int MIN1 = INT_MAX, MIN2 = INT_MAX;
        //遍历找出这5个数
        for(const int num:nums){
            if(num > MAX1){
                MAX3 = MAX2;
                MAX2 = MAX1;
                MAX1 = num;
            }else if(num > MAX2){
                MAX3 = MAX2;
                MAX2 = num;
            }else if(num > MAX3){
                MAX3 = num;
            }
            if(num < MIN1){
                MIN2 = MIN1;
                MIN1 = num;
            }else if(num < MIN2){
                MIN2 = num;
            }
        }
        return max(MAX1*MAX2*MAX3, MAX1*MIN1*MIN2);
    }
}; 
