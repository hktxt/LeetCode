//solution 1:
//https://www.cnblogs.com/grandyang/p/5507129.html
//用个set把nums1都放进去，然后遍历nums2的元素，如果在set中存在，说明是交集的部分，
//加入结果的set中，最后再把结果转为vector的形式即可
class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        set<int> s(nums1.begin(), nums1.end()), res;
        for(auto a : nums2){
            if(s.count(a)) res.insert(a);
        }
        return vector<int>(res.begin(), res.end());
    }
};
//Runtime: 8 ms, faster than 100.00% of C++ online submissions for Intersection of Two Arrays.
//Memory Usage: 9.97 MB, less than 15.56% of C++ online submissions for Intersection of Two Arrays.


//sloution 2:
//https://www.cnblogs.com/grandyang/p/5507129.html
//我们还可以使用两个指针来做，先给两个数组排序，然后用两个指针分别指向两个数组的开头，
//然后比较两个数组的大小，把小的数字的指针向后移，如果两个指针指的数字相等，那么看结果res是否为空，
//如果为空或者是最后一个数字和当前数字不等的话，将该数字加入结果res中
class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        vector<int> res;
        int i = 0, j = 0;
        sort(nums1.begin(), nums1.end());
        sort(nums2.begin(), nums2.end());
        while(i < nums1.size() && j < nums2.size()){
            if(nums1[i] < nums2[j]) i++;
            else if(nums1[i] > nums2[j]) j++;
            else{
                if(res.empty() || res.back() != nums1[i]){
                    res.push_back(nums1[i]);
                }
                i++, j++;
            }
        }
        return res;
    }
};
//Runtime: 8 ms, faster than 100.00% of C++ online submissions for Intersection of Two Arrays.
//Memory Usage: 9 MB, less than 85.16% of C++ online submissions for Intersection of Two Arrays.


//solution 3:
//https://www.cnblogs.com/grandyang/p/5507129.html
//我们还可以使用二分查找法来做，思路是将一个数组排序，然后遍历另一个数组，
//把遍历到的每个数字在排序号的数组中用二分查找法搜索，如果能找到则放入结果set中，
//这里我们用到了set的去重复的特性，最后我们将set转为vector
class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        set<int> res;
        sort(nums2.begin(), nums2.end());
        for(auto a : nums1){
            if(binarySearch(nums2, a)) res.insert(a);
        }
        return vector<int>(res.begin(), res.end());
    }
    bool binarySearch(vector<int> &nums, int target){
        int left = 0, right = nums.size();
        while(left < right){
            int mid = left + (right - left) / 2;
            if(nums[mid] == target) return true;
            else if(nums[mid] < target) left = mid + 1;
            else right = mid;
        }
        return false;
    }
};
//Runtime: 12 ms, faster than 85.74% of C++ online submissions for Intersection of Two Arrays.
//Memory Usage: 9.1 MB, less than 78.04% of C++ online submissions for Intersection of Two Arrays.

//solution 4:
//https://www.cnblogs.com/grandyang/p/5507129.html
//或者我们也可以使用STL的set_intersection函数来找出共同元素
class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        set<int> s1(nums1.begin(), nums1.end()), s2(nums2.begin(), nums2.end()), res;
        set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), inserter(res, res.begin()));
        return vector<int>(res.begin(), res.end());
    }
};
//Runtime: 12 ms, faster than 85.74% of C++ online submissions for Intersection of Two Arrays.
//Memory Usage: 10.1 MB, less than 5.05% of C++ online submissions for Intersection of Two Arrays.
