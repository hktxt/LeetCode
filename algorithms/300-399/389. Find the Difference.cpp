//2019.2.26
//https://www.cnblogs.com/grandyang/p/5816418.html
//用哈希表来建立字符和个数之间的映射，如果在遍历t的时候某个映射值小于0了，那么返回该字符即可
class Solution {
public:
    char findTheDifference(string s, string t) {
        unordered_map<char, int> m;
        for(char c : s) m[c]++;
        for(char c : t){
            if(--m[c] < 0) return c;
        }
        return 0;
    }
};
//Runtime: 12 ms, faster than 24.88% of C++ online submissions for Find the Difference.
//Memory Usage: 9.4 MB, less than 19.01% of C++ online submissions for Find the Difference.


//2019.2.26
//https://www.cnblogs.com/grandyang/p/5816418.html
//也可以使用位操作Bit Manipulation来做，利用异或的性质，相同位返回0，这样相同的字符都抵消了，剩下的就是后加的那个字符
class Solution {
public:
    char findTheDifference(string s, string t) {
        char res = 0;
        for(char c : s) res ^= c;
        for(char c : t) res ^= c;
        return res;
    }
};
//Runtime: 8 ms, faster than 97.07% of C++ online submissions for Find the Difference.
//Memory Usage: 9.2 MB, less than 76.86% of C++ online submissions for Find the Difference.


//2019.2.26
//https://www.cnblogs.com/grandyang/p/5816418.html
//也可以直接用加和减，相同的字符一减一加也抵消了，剩下的就是后加的那个字符
class Solution {
public:
    char findTheDifference(string s, string t) {
        char res = 0;
        for(char c : s) res -= c;
        for(char c : t) res += c;
        return res;
    }
};
//Runtime: 4 ms, faster than 100.00% of C++ online submissions for Find the Difference.
//Memory Usage: 9.3 MB, less than 48.76% of C++ online submissions for Find the Difference.


//2019.2.26
//https://www.cnblogs.com/grandyang/p/5816418.html
//史蒂芬大神提出来的，利用了STL的accumulate函数
class Solution {
public:
    char findTheDifference(string s, string t) {
        return accumulate(begin(s), end(s += t), 0, bit_xor<int>());
    }
};
//Runtime: 4 ms, faster than 100.00% of C++ online submissions for Find the Difference.
//Memory Usage: 9.4 MB, less than 18.18% of C++ online submissions for Find the Difference.
