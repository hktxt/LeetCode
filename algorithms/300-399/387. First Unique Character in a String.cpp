//2019.2.26
//https://www.cnblogs.com/grandyang/p/5802109.html
//用哈希表建立每个字符和其出现次数的映射，然后按顺序遍历字符串，找到第一个出现次数为1的字符，返回其位置
class Solution {
public:
    int firstUniqChar(string s) {
        unordered_map<char, int> m;
        for(char c : s) m[c]++;
        for(int i = 0; i < s.size(); i++){
            if(m[s[i]] == 1) return i;
        }
        return -1;
    }
};
//Runtime: 64 ms, faster than 44.53% of C++ online submissions for First Unique Character in a String.
//Memory Usage: 13.6 MB, less than 43.02% of C++ online submissions for First Unique Character in a String.
