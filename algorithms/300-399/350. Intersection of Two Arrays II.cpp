//2019.2.26
//与349题区别在于，保留重复的数字，参考349的solution 2，先排序，再遍历数组，后移较小的指针，相同则为intersection
class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        vector<int> res;
        int i = 0, j = 0;
        sort(nums1.begin(), nums1.end());
        sort(nums2.begin(), nums2.end());
        while(i < nums1.size() && j < nums2.size()){
            if(nums1[i] < nums2[j]) i++;
            else if(nums1[i] > nums2[j]) j++;
            else{
                res.push_back(nums1[i]);
                i++;
                j++;
            }
        }
        return res;
    }
};
//Runtime: 12 ms, faster than 82.84% of C++ online submissions for Intersection of Two Arrays II.
//Memory Usage: 9.1 MB, less than 78.66% of C++ online submissions for Intersection of Two Arrays II.
