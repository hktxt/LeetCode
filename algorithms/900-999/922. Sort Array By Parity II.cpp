//https://leetcode.com/problems/sort-array-by-parity-ii/solution/
//O(N) O(1)
class Solution {//2019.1.11
public:
    vector<int> sortArrayByParityII(vector<int>& A) {
        for(int j=1, i=0; i<A.size(); i+=2){
            if(A[i] % 2 == 1){
                while(A[j] % 2 == 1){
                    j += 2;
                }
                int tmp = A[i];
                A[i] = A[j];
                A[j] = tmp;
            }
        }
        return A;
    } 
};
