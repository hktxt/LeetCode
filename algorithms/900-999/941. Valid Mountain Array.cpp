//从左往右找出peek1，再从右往左找出peek2， peek1==peek2则true, 56 ms
class Solution {//2019.1.11
public:
    bool validMountainArray(vector<int>& A) {
        if(A.size() < 3) return false;
        int peek1 = -1, peek2 = -1;
        for(int i=0; i<A.size()-1; i++){
            if(A[i] == A[i+1]) return false;
            if(A[i] > A[i+1]){
                peek1 = i;
                break;
            }
        }
        if(peek1 <= 0) return false;
        for(int j=A.size()-1; j>=peek1; j--){
            if(A[j] == A[j-1]) return false;
            if(A[j] > A[j-1]){
                peek2 = j;
                break;
            }
        }
        return peek1 == peek2;
    }
};

//https://leetcode.com/problems/valid-mountain-array/discuss/212791/c%2B%2B-6-lines, 44 ms
class Solution {
public:
    bool validMountainArray(vector<int>& A) {
        if(A.size() < 3) return false;
        
        int i=0, j = A.size() - 1;
        while(i+1 < A.size() && A[i]<A[i+1]) ++i;
        while(j-1 >=0 && A[j] < A[j-1]) --j;
        return i==j && i>0 && j<A.size()-1;
    }
};
