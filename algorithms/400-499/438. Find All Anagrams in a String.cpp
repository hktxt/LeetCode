//2019.2.28
//https://www.cnblogs.com/grandyang/p/6014408.html
//在s中找字符串p的所有变位次的位置，所谓变位次就是字符种类个数均相同但是顺序可以不同的两个词，
//那么我们肯定首先就要统计字符串p中字符出现的次数，然后从s的开头开始，每次找p字符串长度个字符，
//来验证字符个数是否相同，如果不相同出现了直接break，如果一直都相同了，则将起始位置加入结果res中
class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        if(s.empty()) return {};
        vector<int> res, cnt(128, 0);
        int ns = s.size(), np = p.size(), i = 0;
        for(char c : p) cnt[c]++;
        while(i < ns){
            bool sucess = true;
            vector<int> tmp = cnt;
            for(int j = i; j < i+np; j++){
                if(--tmp[s[j]] < 0){
                    sucess = false;
                    break;
                }
            }
            if(sucess) res.push_back(i);
            i++;
        }
        return res;
    }
};
//Runtime: 1200 ms, faster than 10.14% of C++ online submissions for Find All Anagrams in a String.
//Memory Usage: 102.3 MB, less than 5.00% of C++ online submissions for Find All Anagrams in a String.


//2019.2.28
//https://www.cnblogs.com/grandyang/p/6014408.html
//用两个哈希表，分别记录p的字符个数，和s中前p字符串长度的字符个数，然后比较，如果两者相同，
//则将0加入结果res中，然后开始遍历s中剩余的字符，每次右边加入一个新的字符，然后去掉左边的一个旧的字符，
//每次再比较两个哈希表是否相同即可
class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        if(s.empty()) return {};
        vector<int> res, m1(256, 0), m2(256, 0);
        for(int i=0; i < p.size(); i++){
            m1[s[i]]++;
            m2[p[i]]++;
        }
        if(m1 == m2) res.push_back(0);
        for(int i = p.size(); i < s.size(); i++){
            m1[s[i]]++;
            m1[s[i - p.size()]]--;
            if(m1 == m2) res.push_back(i - p.size() + 1);
        }
        return res;
    }
};
//Runtime: 36 ms, faster than 90.96% of C++ online submissions for Find All Anagrams in a String.
//Memory Usage: 10.5 MB, less than 76.82% of C++ online submissions for Find All Anagrams in a String.
