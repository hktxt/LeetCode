//dp, 记录当前最长N和全局最长M，若N大于M，则M=N,返回M
class Solution {//2018.12.21
public:
    int findMaxConsecutiveOnes(vector<int>& nums) {
        int N = 0, M = 0;
        for(int i=0; i<nums.size(); i++){
            if(nums[i] == 1) N++;
            else N = 0;
            if(N > M) M = N;
        }
        return M;
    }
};
