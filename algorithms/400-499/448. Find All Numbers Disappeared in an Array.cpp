/**
[4,3,2,7,8,2,3,1]:

0. [4, 3, 2, -7, 8, 2, 3, 1]
1. [4, 3, -2, -7, 8, 2, 3, 1]
2. [4, -3, -2, -7, 8, 2, 3, 1]
3. [4, -3, -2, -7, 8, 2, -3, 1]
4. [4, -3, -2, -7, 8, 2, -3, -1]
5. [4, -3, -2, -7, 8, 2, -3, -1]
6. [4, -3, -2, -7, 8, 2, -3, -1]
7. [-4, -3, -2, -7, 8, 2, -3, -1]
不存在的数，也即下标，则不会被转负。

**/
class Solution {//2018.12.21
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        vector<int> res;
        for(int i=0; i<nums.size(); i++){
            nums[abs(nums[i]) - 1] = -1*abs(nums[abs(nums[i]) - 1]);//将nums中的值以其下标表示为负
        }
        for(int i=0; i<nums.size(); i++){
            if(nums[i] > 0){
                res.push_back(i+1);
            }
        } 
        return res;
    }
};
