//2019.2.28
//https://www.cnblogs.com/grandyang/p/5931874.html
//给了我们一个字符串，让我们找出可以组成的最长的回文串的长度，由于字符顺序可以打乱，
//所以问题就转化为了求偶数个字符的个数，我们了解回文串的都知道，回文串主要有两种形式，
//一个是左右完全对称的，比如noon, 还有一种是以中间字符为中心，左右对称，比如bob，level等，
//那么我们统计出来所有偶数个字符的出现总和，然后如果有奇数个字符的话，我们取取出其最大偶数，然后最后结果加1即可
class Solution {
public:
    int longestPalindrome(string s) {
        unordered_map<char, int> m;
        int res = 0;
        bool even = false;
        for(char c : s) m[c]++;
        for(auto it = m.begin(); it != m.end(); it++){
            res += it->second;
            if(it->second % 2 == 1) {
                res -= 1;
                even = true;
            }
        }
        return even ? res + 1 : res;
    }
};
//Runtime: 8 ms, faster than 100.00% of C++ online submissions for Longest Palindrome.
//Memory Usage: 9.3 MB, less than 81.90% of C++ online submissions for Longest Palindrome.
