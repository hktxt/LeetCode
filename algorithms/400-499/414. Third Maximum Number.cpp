//
class Solution {//2018.12.21
public:
    int thirdMax(vector<int>& nums) {
        long first = LONG_MIN, second = LONG_MIN, third = LONG_MIN; //定义3个最大的数为最小LONG_MIN
        for(auto num:nums){
            if(first == num || second == num || third == num) continue; //如果num在3个数中，继续
            
            if(num > first){ //如果num大于最大的数，则最大数为num，余下后移
                third = second;
                second = first;
                first = num;
            }
            else if(num > second){//如果num大于第二大的数，则第二数为num，余下后移
                third = second;
                second = num;
            }
            else if(num > third){//如果num大于第三的数，则第三数为num
                third = num;
            }
        }
        return third == LONG_MIN? first : third;
    }
};
