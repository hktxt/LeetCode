//2019.2.28
//https://www.youtube.com/watch?v=ddFvTWmVUEA
//https://www.cnblogs.com/grandyang/p/6096138.html
//count岛的数量，如果岛没有与其他岛相连则边长为4，如果相连一个则少两条边
class Solution {
public:
    int islandPerimeter(vector<vector<int>>& grid) {
        if(grid.empty()) return 0;
        int m = grid.size();
        int n = grid[0].size();
        int con = 0, area = 0;
        
        for(int y=0; y<m; y++){
            for(int x=0; x<n; x++){
                if(grid[y][x] == 1){
                    area++;
                    if(y>0 && grid[y-1][x] == 1) con++;
                    if(y<m-1 && grid[y+1][x] == 1) con++;
                    if(x>0 && grid[y][x-1] == 1) con++;
                    if(x<n-1 && grid[y][x+1] == 1) con++;
                }
            }
        }
        return area*4 - con;
    }
};
//Runtime: 132 ms, faster than 99.30% of C++ online submissions for Island Perimeter.
//Memory Usage: 49 MB, less than 71.70% of C++ online submissions for Island Perimeter.
    
