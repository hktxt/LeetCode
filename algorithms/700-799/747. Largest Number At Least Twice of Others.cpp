//https://www.youtube.com/watch?v=hxCyIS1ZDLY
//just walk through the array, and find out Max and second Max values, then compare Max >= 2*secMax
class Solution {//2019.1.7
public:
    int dominantIndex(vector<int>& nums) {
        int Maxval = -1;
        int secMax = -1;
        int index = -1;
        for(int i=0; i<nums.size(); i++){
            if(nums[i] > Maxval){
                int tmp = Maxval;
                Maxval = nums[i];
                secMax = tmp;
                index = i;
            }else if(nums[i] > secMax){
                secMax = nums[i];
            }
        }
        if(Maxval >= 2*secMax) return index;
        else return -1;
    }
};
