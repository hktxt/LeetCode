//https://leetcode.com/problems/toeplitz-matrix/discuss/209271/C%2B%2B-4ms-solution-100
class Solution {//2019.1.7
public:
    bool isToeplitzMatrix(vector<vector<int>>& matrix) {
        int R = matrix.size();
        int C = matrix[0].size();
        for(int r=0; r<R-1; r++){
            for(int c=0; c<C-1; c++){
                if(matrix[r][c] != matrix[r+1][c+1]) return false;
            }
        }
        return true;
    }
};
