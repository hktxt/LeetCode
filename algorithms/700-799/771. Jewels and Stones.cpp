//2019.3.8
//https://www.cnblogs.com/grandyang/p/8910994.html
//暴力搜索吧，就将S中的每个字符都在J中搜索一遍，搜索到了就break掉
class Solution {
public:
    int numJewelsInStones(string J, string S) {
        int res=0;
        for(char s : S){
            for(char j : J){
                if(s == j){
                    res++;
                    break;
                }
            }
        }
        return res;
    }
};
//Runtime: 12 ms, faster than 10.34% of C++ online submissions for Jewels and Stones.
//Memory Usage: 9.9 MB, less than 51.35% of C++ online submissions for Jewels and Stones.


//2019.3.8
//https://www.cnblogs.com/grandyang/p/8910994.html
//用HashSet来优化时间复杂度，将珠宝字符串J中的所有字符都放入HashSet中，然后遍历石头字符串中的每个字符，到HashSet中查找是否存在，存在的话计数器自增1即可
class Solution {
public:
    int numJewelsInStones(string J, string S) {
        int res = 0;
        unordered_set<char> SET;
        for(char j : J) SET.insert(j);
        for(char s : S) res += SET.count(s);
        
        return res;
    }
};
//Runtime: 8 ms, faster than 81.66% of C++ online submissions for Jewels and Stones.
//Memory Usage: 10.1 MB, less than 36.78% of C++ online submissions for Jewels and Stones.
