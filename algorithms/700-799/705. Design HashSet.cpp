//2019.3.4
//http://www.cnblogs.com/grandyang/p/9966807.html
class MyHashSet {
public:
    /** Initialize your data structure here. */
    MyHashSet() {
        data.resize(1000000, 0);
    }
    
    void add(int key) {
        data[key] = 1;
    }
    
    void remove(int key) {
        data[key] = 0;
    }
    
    /** Returns true if this set contains the specified element */
    bool contains(int key) {
        return data[key] == 1;
    }
    
    private:
    vector<int> data;
};

/**
 * Your MyHashSet object will be instantiated and called as such:
 * MyHashSet obj = new MyHashSet();
 * obj.add(key);
 * obj.remove(key);
 * bool param_3 = obj.contains(key);
 */


//Runtime: 168 ms, faster than 31.78% of C++ online submissions for Design HashSet.
//Memory Usage: 158.9 MB, less than 9.52% of C++ online submissions for Design HashSet.
