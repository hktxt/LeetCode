//2018.12.29
//https://leetcode.com/problems/shortest-unsorted-continuous-subarray/discuss/123732/Two-c%2B%2B-solution
//https://www.youtube.com/watch?v=Hg03KTli9co
//从前开始寻找最大值， 如果遇到元素小于其前面的最大值， 则确定为右边界。
//从后开始寻找最小值， 如遇到到元素大于其后面的最小值，则确定为左边界。
//初始化l=-1, r =-2，则为确保最短的情况 （原数组递增）为零。
class Solution {
public:
    int findUnsortedSubarray(vector<int>& nums) {
        int n= nums.size(),  l=-1, r =-2, mn=nums[n-1], mx=nums[0];

        for(int i=0; i<n; ++i){
            mx = max(nums[i], mx);
            if(nums[i] < mx) r = i;

            mn = min (nums[n-1-i], mn);
            if(nums[n-1-i] >mn) l=n-1-i;

        }
        return r-l+1;
    }
};
