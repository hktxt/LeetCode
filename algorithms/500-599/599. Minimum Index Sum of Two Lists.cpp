//2019.2.28
//http://www.cnblogs.com/grandyang/p/6978646.html
//这道题给了我们两个字符串数组，让我们找到坐标位置之和最小的相同的字符串。那么对于这种数组项和其坐标之间关系的题，最先考虑到的就是要建立数据和其位置坐标之间的映射。我们建立list1的值和坐标的之间的映射，然后遍历list2，如果当前遍历到的字符串在list1中也出现了，那么我们计算两个的坐标之和，如果跟我们维护的最小坐标和mn相同，那么将这个字符串加入结果res中，如果比mn小，那么mn更新为这个较小值，然后将结果res清空并加入这个字符串
class Solution {
public:
    vector<string> findRestaurant(vector<string>& list1, vector<string>& list2) {
        vector<string> res;
        unordered_map<string, int> m;
        int mn = INT_MAX, n1 = list1.size(), n2 = list2.size();
        for(int i=0; i<n1; i++) m[list1[i]] = i;
        for(int i=0; i<n2; i++){
            if(m.count(list2[i])){
                int sum = i + m[list2[i]];
                if(sum == mn) res.push_back(list2[i]);
                else if(sum < mn){
                    mn = sum;
                    res = {list2[i]};
                }
            }
        }
        return res;
    }
};
//Runtime: 112 ms, faster than 80.58% of C++ online submissions for Minimum Index Sum of Two Lists.
//Memory Usage: 28.9 MB, less than 36.46% of C++ online submissions for Minimum Index Sum of Two Lists.
