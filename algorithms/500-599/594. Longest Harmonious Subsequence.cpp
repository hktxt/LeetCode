//2019.2.28
//http://www.cnblogs.com/grandyang/p/6896799.html
//最长的和谐子序列，关于和谐子序列就是序列中数组的最大最小差值均为1。由于这里只是让我们求长度，并不需要返回具体的子序列。所以我们可以对数组进行排序，那么实际上我们只要找出来相差为1的两个数的总共出现个数就是一个和谐子序列的长度了。明白了这一点，我们就可以建立一个数字和其出现次数之间的映射，利用map的自动排序的特性，那么我们遍历map的时候就是从小往大开始遍历，我们从第二个映射对开始遍历，每次跟其前面的映射对比较，如果二者的数字刚好差1，那么就把二个数字的出现的次数相加并更新结果res即可
class Solution {
public:
    int findLHS(vector<int>& nums) {
        if(nums.empty()) return 0;
        int res = 0;
        map<int, int> m;
        for(int num : nums) m[num]++;
        for(auto it = next(m.begin()); it != m.end(); it++){
            auto pre = prev(it);
            if(it->first == pre->first + 1){
                res = max(res, it->second + pre->second);
            }
        }
        return res;
    }
};
//Runtime: 132 ms, faster than 41.33% of C++ online submissions for Longest Harmonious Subsequence.
//Memory Usage: 21.9 MB, less than 32.08% of C++ online submissions for Longest Harmonious Subsequence.
