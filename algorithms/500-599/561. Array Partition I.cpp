/**2018.12.24
min最大化，只有两个最近的数组合，才能保证；先排序，然后取每个组合里最小的值

class Solution {//solution 1, sort
public:
    int arrayPairSum(vector<int>& nums) {
        //O(nlogn)
        std::sort(nums.begin(), nums.end());
        int ans = 0;
        for(int i=0; i<nums.size(); i+=2){
            ans += nums[i];
        }
        return ans;
    }
};

**/

class Solution {//solution 2, 数组，
public:
    int arrayPairSum(vector<int>& nums) {
        const int kMaxvalue = 10000;
        array<int, 2*kMaxvalue+1> c{};
        
        for(int num : nums) ++c[num + kMaxvalue];
        
        int ans = 0;
        int n = -kMaxvalue;
        bool first = true;
        
        while(n <= kMaxvalue){
            if(!c[n + kMaxvalue]){
                ++n;
                continue;
            }
            if(first){
                ans += n;
                first = false;
            }else{
                //skip the element
                first = true;
            }
            --c[n + kMaxvalue]; 
        }
        return ans;
    }
};
    
