class Solution {//2018/12.24
public:
    vector<vector<int>> matrixReshape(vector<vector<int>>& nums, int r, int c) {
        vector<vector<int>> matrix;
        vector<int> new_row;
        int row_of_nums = nums.size();
        int col_of_nums = nums[0].size();
        if(row_of_nums * col_of_nums != r * c){
            matrix = nums;
            return matrix;
        }
        for(int i=0; i<row_of_nums; i++){
            for(int j=0; j<col_of_nums; j++){
                new_row.push_back(nums[i][j]);
                if((i*col_of_nums + j)%c == c-1){
                    matrix.push_back(new_row);
                    new_row.clear();
                } 
            }
        }
        return matrix;
    }
};
