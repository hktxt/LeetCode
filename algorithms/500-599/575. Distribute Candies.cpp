//2019.2.28
//http://www.cnblogs.com/grandyang/p/6847675.html
//这道题给我们一堆糖，每种糖的个数不定，分给两个人，让我们求其中一个人能拿到的最大的糖的种类数。
//那么我们想，如果总共有n个糖，平均分给两个人，每人得到n/2块糖，那么能拿到的最大的糖的种类数也就是n／2种，
//不可能再多，只可能再少。那么我们要做的就是统计出总共的糖的种类数，如果糖的种类数小于n/2，说明拿不到n/2种糖，
//最多能拿到的种类数数就是当前糖的总种类数，明白了这点就很容易了，我们利用集合set的自动去重复特性来求出糖的种类数，
//然后跟n/2比较，取二者之中的较小值返回即可
class Solution {
public:
    int distributeCandies(vector<int>& candies) {
        unordered_set<int> s;
        for(int candy : candies) s.insert(candy);
        return min(s.size(), candies.size()/2);
    }
};
//Runtime: 316 ms, faster than 42.66% of C++ online submissions for Distribute Candies.
//Memory Usage: 52.2 MB, less than 33.87% of C++ online submissions for Distribute Candies.


//2019.2.28
//https://leetcode.com/problems/distribute-candies/discuss/226080/C%2B%2B-solution-in-2-lines
class Solution {
public:
    int distributeCandies(vector<int>& candies) {
        unordered_set<int> s(candies.begin(), candies.end());
        return min(s.size(), candies.size() / 2);
    }
};
//Runtime: 300 ms, faster than 62.24% of C++ online submissions for Distribute Candies.
//Memory Usage: 45.9 MB, less than 75.81% of C++ online submissions for Distribute Candies.
