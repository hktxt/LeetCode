//https://leetcode.com/problems/valid-anagram/discuss/66519/2-C%2B%2B-Solutions-with-Explanations
class Solution {//2019.1.17
public:
    bool isAnagram(string s, string t) {
        if(s.size() != t.size()) return false;
        int n = s.size();
        unordered_map<char, int> counts;
        for(int i=0; i<n;i++){
            counts[s[i]]++;
            counts[t[i]]--;
        }
        for(auto count : counts){
            if(count.second) return false;
        }
        return true;
        
    }
};
