//https://www.youtube.com/watch?v=8f2Etj_slPo
/*
happy number 19->82->68->100->1
unhappy number 200->4->16->37->58->89->145->42->20->4
               slow
               fast
*/
class Solution {//2019.1.15
public:
    int digitSquareSum(int n){
        int sum = 0;
        int tmp;
        while(n){
            tmp = n % 10;
            sum += tmp * tmp;
            n /= 10;
        }
        return sum;
    }
    
    bool isHappy(int n) {
        int slow = n;
        int fast = n;
        do{
            slow = digitSquareSum(slow);
            fast = digitSquareSum(fast);
            fast = digitSquareSum(fast);
        }while(slow != fast);
        return slow == 1;
    }
};

//solution 2
class Solution {//2019.1.15
public:
    
    int digitSquareSum(int n){
        int sum=0, tmp;
        while(n){
            tmp = n % 10;
            sum += tmp * tmp;
            n /= 10;
        }
        return sum;
    }
    bool isHappy(int n) {
        unordered_set<int> s;
        s.insert(1);
        while(s.find(n) == s.end()){//http://www.cplusplus.com/reference/unordered_set/unordered_set/find/
            s.insert(n);
            n = digitSquareSum(n);
        }
        return n == 1;
    }
};
/*
Searches the container for an element with k as value and returns an iterator to it if found,
otherwise it returns an iterator to unordered_set::end (the element past the end of the container).
*/
