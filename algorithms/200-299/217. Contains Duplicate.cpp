/**why this does not work?
class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {
        int len = nums.size();
        if(len <= 1) return false;
        
        vector<int> tmp(len);
        for(int i=0; i<len; i++){
            tmp[nums[i]]++;
            if(tmp[nums[i] > 1]) return true;
        }
        return false;
    }
};
**/
class Solution {//2018.12.06
public:
    bool containsDuplicate(vector<int>& nums) {
        
        if(nums.size() <= 1) return false;
        
        unordered_set<int> set;
        for(int i=0; i<nums.size(); i++){
            set.insert(nums[i]);
            if(set.size() < (i+1)) return true;
        }
        return false;
    }
};
