/**
https://leetcode.com/problems/move-zeroes/
从前向后扫描，将不为0的元素前移，再将后面赋0.
**/
class Solution {//2018/12/20
public:
    void moveZeroes(vector<int>& nums) {
        int n = nums.size();
        int count = 0;
        for(int i=0; i<n; i++){
            if(nums[i] != 0){                 //如果不为0，
                nums[count++] = nums[i];      //将不为0的元素前移，赋给nums[count++]
            }
        }
        while(count < n){                     //余下赋0
            nums[count++] = 0;
        }
    }
};
