//https://leetcode.com/problems/word-pattern/discuss/218286/C%2B%2B-Solution-beats-100-with-explanatory-comments
class Solution {//2019.1.21
public:
    bool wordPattern(string pattern, string str) {
        vector<string> words;
        string word;
        istringstream stream(str);
        
        while(stream >> word) words.push_back(word);
        
        if(pattern.size() != words.size()) return false;
        
        unordered_map<char, string> hmp;
        unordered_set<string> SET;
        
        for(int i=0; i<pattern.size(); i++){
            char c = pattern[i];
            word = words[i];
            
            if(hmp.find(c) != hmp.end() && hmp[c] != word) return false;
            if(hmp.find (c) == hmp.end() && SET.find(word) != SET.end()) return false;
            
            hmp[c] = word;
            SET.insert(word);
        }
        return true;
    }
};
