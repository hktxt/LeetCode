class Solution {//2018.12.07
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k) {
        if(nums.size() < 2 || k == 0) return false;
        std::unordered_set<int> window;
        for(int i=0; i<nums.size(); i++){
            if(!window.insert(nums[i]).second) return true;
            if(i >= k) window.erase(nums[i-k]);
        }
        return false;
    }
};
