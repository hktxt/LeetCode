//https://leetcode.com/problems/isomorphic-strings/discuss/208778/C%2B%2B-9-lines-simple-solution
class Solution {//2019.116
public:
    bool isIsomorphic(string s, string t) {
        unordered_map<char, vector<int>> hashmap_s;
        unordered_map<char, vector<int>> hashmap_t;
        for(int i=0; i<s.size(); i++){
            hashmap_s[s[i]].push_back(i);
            hashmap_t[t[i]].push_back(i);
        }
        for(int i=0; i<s.size(); i++){
            if(hashmap_s[s[i]].size() == 1 && hashmap_t[t[i]].size() == 1) continue;
            if(hashmap_s[s[i]] != hashmap_t[t[i]]) return false;
        }
        return true;
    }
};
