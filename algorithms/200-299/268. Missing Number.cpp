/***
solution form: https://leetcode.com/problems/missing-number/discuss/69832/1%2B-lines-Ruby-Python-Java-C%2B%2B
"Sum of 0 to n minus sum of the given numbers is the missing one." 从0到n求和减去给定的数组和结果即为缺少的值，牛逼。
***/
// [3,0,1].size() = 3; so 1+2+3=6, sum([3,0,1]) = 4; so 6-4=2. 如果，nums不从0开始，而从1开始，稍微修改下即可。


class Solution {
public:
    int missingNumber(vector<int>& nums) {
        return 0.5 * nums.size() * (nums.size() + 1) - accumulate(nums.begin(), nums.end(), 0);
    }
};
