class Solution {//2018.12.04 
public:
    vector<int> getRow(int rowIndex) {
        auto res = vector<int>{1};
        for(int i=0; i < rowIndex; i++){
            auto next = vector<int>{1};
            for(int j=0; j<res.size()-1; j++){
                next.push_back(res[j] + res[j + 1]);
            }
            next.push_back(1);
            res = next;
        }
        return res;
    }
};
