class Solution {//2018.12.04
public:
    vector<vector<int>> generate(int numRows) {
        auto res = vector<vector<int>>{};
        auto next = vector<int>{1};
        for(int i=0; i < numRows; i++){
            res.push_back(next);
            auto newNext = vector<int>{1};
            for(int j=0; j<next.size()-1; j++){
                newNext.push_back(next[j] + next[j + 1]);
            }
            newNext.push_back(1);
            next = newNext;
        }
        return res;
    }
};

