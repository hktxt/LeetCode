class Solution {//2018.12.05
public: 
    vector<int> twoSum(vector<int>& numbers, int target) {
        vector<int> res;
        for(int l=0, r=numbers.size()-1; l <= r; l++){
            for(; l<r; r--){
                int a = numbers[l], b = numbers[r];
                if(a+b <= target) break;
            }
            int a = numbers[l], b = numbers[r];
            if(a+b == target){
                res.push_back(l+1);
                res.push_back(r+1);
                break;
            }
        }
        return res;
    }
};
