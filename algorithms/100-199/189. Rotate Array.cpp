class Solution {//2018.12.06 
//https://www.youtube.com/watch?v=utE_1ppU5DY
public:
    int gcd(int a, int b){
        if(b==0) return a;
        else return gcd(b, a%b);
    }
    void rotate(vector<int>& nums, int k) {
        int n = nums.size();
        if (k == 0 || n <= 1) return;
        if (k <= n) k = n-k;
        else k = 2*n - k;
        for(int i=0; i<gcd(n, k); i++){
            int j = i;
            int tmp = nums[i];
            while(1){
                int d = (j+k)%n;
                if(d == i) break;
                nums[j] = nums[d];
                j = d;
            }
            nums[j] = tmp;
        }
    }
};
