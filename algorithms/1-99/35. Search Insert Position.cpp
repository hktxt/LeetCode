class Solution {//2018.12.04 
public:
    int searchInsert(vector<int>& nums, int target) {
        
        if(nums.size() == 0) return 0;
        if(nums[0] >= target) return 0;
        if(nums[nums.size()-1] < target) return nums.size();
        
        for(int i=1; i<nums.size(); i++){
            if(nums[i] == target) return i;
            if(nums[i-1] < target && nums[i] > target) return i;
        }
    }
};
