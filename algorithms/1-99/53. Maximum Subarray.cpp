class Solution {//2018.12.04 
public:
    int maxSubArray(vector<int>& nums) {
        
        if(nums.size() == 0) return 0;
        
        vector<int> f(nums.size());
        f[0] = nums[0];
        for(int i=1; i<nums.size();i++){
            f[i] = max(f[i-1] + nums[i], nums[i]);
        }
        
        return *std::max_element(f.begin(), f.end());
    }
};

//================================================================//
class Solution {//2018.12.04
public:
    int maxSubArray(vector<int>& nums) {
        
        if(nums.size() == 0) return 0;
        
        int ans = nums[0], sum = nums[0];
        for(int i=1; i<nums.size();i++){
            sum = max(sum + nums[i], nums[i]);
            if(sum > ans) ans = sum;
        }
        
        return ans;
    }
};
